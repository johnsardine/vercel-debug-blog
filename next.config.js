const assetPrefix = process.env.ASSET_PREFIX || null;
const basePath = process.env.BASE_PATH || "";

module.exports = {
  assetPrefix,
  basePath,
  reactStrictMode: true,
};
